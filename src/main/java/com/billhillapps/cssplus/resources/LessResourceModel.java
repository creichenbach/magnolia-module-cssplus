package com.billhillapps.cssplus.resources;

import javax.jcr.Node;

import info.magnolia.jcr.util.ContentMap;
import info.magnolia.module.templatingkit.functions.STKTemplatingFunctions;
import info.magnolia.module.templatingkit.resources.STKResourceModel;
import info.magnolia.rendering.model.RenderingModel;
import info.magnolia.rendering.template.RenderableDefinition;
import info.magnolia.templating.functions.TemplatingFunctions;

public class LessResourceModel<RD extends RenderableDefinition> extends
		STKResourceModel<RD> {

	public LessResourceModel(Node content, RD definition,
			RenderingModel<?> parent, STKTemplatingFunctions stkFunctions,
			TemplatingFunctions templatingFunctions) {
		super(content, definition, parent, stkFunctions, templatingFunctions);
	}

	@Override
	public Node getNode() {
		return super.getNode();
	}

	@Override
	public ContentMap getContent() {
		return super.getContent();
	}
	
	@Override
	public String execute() {
		return super.execute();
	}

}
