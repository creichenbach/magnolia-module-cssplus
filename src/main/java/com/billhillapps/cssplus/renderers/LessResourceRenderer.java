package com.billhillapps.cssplus.renderers;

import info.magnolia.freemarker.FreemarkerHelper;
import info.magnolia.jcr.util.PropertyUtil;
import info.magnolia.module.resources.ResourcesModule;
import info.magnolia.module.resources.loaders.ResourceLoader;
import info.magnolia.module.resources.renderers.ResourcesTextTemplateRenderer;
import info.magnolia.rendering.context.RenderingContext;
import info.magnolia.rendering.engine.RenderException;
import info.magnolia.rendering.engine.RenderingEngine;
import info.magnolia.rendering.template.RenderableDefinition;
import info.magnolia.rendering.util.AppendableWriter;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.jcr.Node;

import org.apache.commons.io.IOUtils;
import org.lesscss.LessCompiler;
import org.lesscss.LessException;

public class LessResourceRenderer extends ResourcesTextTemplateRenderer {

	@Inject
	public LessResourceRenderer(FreemarkerHelper fmRenderer,
			RenderingEngine renderingEngine) {
		super(fmRenderer, renderingEngine);
	}

	@Override
	protected void onRender(Node content, RenderableDefinition definition,
			RenderingContext renderingCtx, Map<String, Object> ctx,
			String templateScript) throws RenderException {
		String text = getText(content, templateScript);

		try {
			LessCompiler lessCompiler = new LessCompiler();
			String result = lessCompiler.compile(text);

			AppendableWriter out = renderingCtx.getAppendable();
			out.write(result);
		} catch (LessException e) {
			throw new RenderException(e);
		} catch (IOException e) {
			throw new RenderException(e);
		}
	}

	// FIXME: Code stolen from parent class (necessary due to bad architecture)
	private String getText(Node content, String templateScript)
			throws RenderException {
		StringBuffer text;
		if (shouldBypass(content)) {

			InputStream in = null;
			List<ResourceLoader> loaders = ResourcesModule.getInstance()
					.getResourceLoaders();
			try {
				for (ResourceLoader loader : loaders) {
					in = loader.getStream(templateScript);
					if (in != null) {
						break;
					}
				}
				if (in == null) {
					throw new RenderException("Template " + templateScript
							+ " not found.");
				}
				text = new StringBuffer();
				text.append(IOUtils.toString(in));
			} catch (IOException e) {
				throw new RenderException("Can't render resource", e);
			} finally {
				IOUtils.closeQuietly(in);
			}
		} else {
			text = new StringBuffer();
			text.append(PropertyUtil.getString(content, "text"));
		}

		return text.toString();
	}
}
